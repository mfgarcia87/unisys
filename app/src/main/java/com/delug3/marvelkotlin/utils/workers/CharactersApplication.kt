package com.delug3.marvelkotlin.utils.workers

import android.app.Application
import com.delug3.marvelkotlin.data.persistence.AppDatabase
import com.delug3.marvelkotlin.data.repository.CharactersDatabaseDataSource

class CharactersApplication : Application() {
        // Using by lazy so the database and the repository are only created when they're needed
        // rather than when the application starts
        val database by lazy { AppDatabase.getDatabase(this) }
        val repository by lazy { CharactersDatabaseDataSource(database.charactersDao()) }
}
